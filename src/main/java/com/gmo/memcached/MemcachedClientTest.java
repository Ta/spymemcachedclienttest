package com.gmo.memcached;

public class MemcachedClientTest {

	static String[] keys = null;
	static int numKeys = 10;
	static String keyPrefix = new String("key_memcached");

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Hello World!");
		
		keys = new String[numKeys];
		
		
		
		// Set
		for( int i = 0; i < numKeys ; i++ ){
			keys[i] = i + keyPrefix;
//			Thread.sleep(10000);
			MemCacheUtils.setWithSerialize(i + keyPrefix, 12000, "value[--" + i + "--]");
		}
		
//		delAll();
		// Get
		System.out.println(MemCacheUtils.gets(keys).toString());
		System.out.println(MemCacheUtils.getClient().getAvailableServers().toString());
//		MemCacheUtils.getClient().connectionLost(sa);
//		MemCacheUtils.setWithSerialize("1long", 12000, "aaaaaaaaaaaaaaaaaa");
		Thread.sleep(1000);
		MemCacheUtils.close();

	}

	public static void delAll() {
		for (int i =0 ; i < numKeys ; i++){
			MemCacheUtils.delete(keys[i]);
		}
	}
}
