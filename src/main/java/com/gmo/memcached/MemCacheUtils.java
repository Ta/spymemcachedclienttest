package com.gmo.memcached;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.CachedData;
import net.spy.memcached.ConnectionFactoryBuilder;
import net.spy.memcached.ConnectionFactoryBuilder.Locator;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.transcoders.Transcoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * MemcacheUtils
 *
 * @author Taketo Matsuura
 */
public class MemCacheUtils {

	private static final Log log = LogFactory.getLog(MemCacheUtils.class);
	
    private static MemcachedClient client = null;
    public static MemcachedClient getClient() {
		return client;
	}

	public static void setClient(MemcachedClient client) {
		MemCacheUtils.client = client;
	}

	static {
        try {
            MemcachedClient c = new MemcachedClient(
                    new ConnectionFactoryBuilder().setLocatorType(Locator.CONSISTENT).build(),
                    AddrUtil.getAddresses("192.168.23.187:11211,192.168.23.211:11211,192.168.23.212:11211"));
//            MemcachedClient c = new MemcachedClient(
//                    new ConnectionFactoryBuilder().setLocatorType(Locator.CONSISTENT).build(),
//                    AddrUtil.getAddresses("192.168.23.212:11211"));
            client = c;
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
    }

    private static final int byteMaxSize = 2 * 1024 * 1024;

    private static final Transcoder<CachedData> t = new Transcoder<CachedData>() {


        public boolean asyncDecode(CachedData c) {
            return false;
        }

        public CachedData decode(CachedData c) {
            return c;
        }

        public CachedData encode(CachedData c) {
            return null;
        }

        public int getMaxSize() {
            return byteMaxSize;
        }
    };

    /**
     * Memcachedサーバからkeyに該当するデータを取得する。
     *
     * @param key
     * @return 取得されたオブジェクト
     */
    private static CachedData get(String key) {
        try {
            return client.get(key, t);
        } catch (Throwable e) {
//            log.error("failed to get from memcache server key [" + key + "]", e);
            return null;
        }
    }

    /**
     * Memcachedサーバから複数キーに該当するオブジェクトを取得する。
     * <p>
     * エラーの場合はnull、取得がない場合は実装依存。
     * 必ずnullチェックを実施すること。
     *
     * @param key
     * @return Map<String, Object>
     */
    public static Map<String, Object> gets(String[] keys) {
        try {
            return client.getBulk(keys);
        } catch (Throwable e) {
            log.error("failed to get from memcache server keys [" + Arrays.toString(keys) + "]", e);
            return null;
        }
    }

    /**
     * 指定された値を非同期で設定する。
     *
     * @param key
     * @param expire
     * @param value
     */
    private static void asyncSet(String key, int expire, Object value) {
        try {
            client.set(key, expire, value);
        } catch (Throwable e) {
            log.error("failed to set to memcache server key [" + key + "]", e);
        }
    }

    /**
     * Memcachedサーバから復元（deserialize）された形式で取得する。
     *
     * @param key
     * @return 取得されたオブジェクト
     */
    public static <T> T getWithDeserialize(String key, Class<T> c) {
        CachedData o = get(key);
        if (o == null) {
            return null;
        }

        try {
//            return PDMPSerializer.deserialize(o.getData(), c);
        	return null;
        } catch (Throwable e) {
            log.error("failed to deserialize key[" + key + "]. erase value.", e);
            delete(key);
            return null;
        }
    }

    /**
     * Memcachedサーバにシリアライズした形式で保存する。
     *
     * @param key
     * @param expire
     * @param value
     */
    public static void setWithSerialize(String key, int expire, Object value) {
        byte[] o;
//        try {
//            o = PDMPSerializer.serialize(value);
//        } catch (Throwable e) {
//            log.error("failed to serialize object for key [" + key + "]. not set.", e);
//            return;
//        }
        asyncSet(key, expire, value);
    }

    /**
     * 指定された値を非同期で設定する。
     *
     * @param key
     * @param expire
     * @param value
     */
    public static void delete(String key) {
        try {
            client.delete(key);
        } catch (Throwable e) {
            log.error("failed to delete key [" + key + "] of memcache server", e);
        }
    }

    /**
     * MemcachedClientを生成する。
     *
     * @return 生成されたMemcachedClient
     */
    public static void close() {
        if (client != null) {
            try {
                client.shutdown();
            } catch (Throwable e) {
                log.error("failed to close memcached client!", e);
            }
        }
    }
}